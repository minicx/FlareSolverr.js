# Contributing to FlareSolverr.js

Thank you for your interest in contributing to FlareSolverr.js! We welcome any contributions that can help improve the project and make it more useful for the community.

## Getting Started

1. Fork the repository and clone it to your local machine.

2. Install project dependencies:
   
   ```bash
   npm install // or yarn install
   ```
3. Make your desired changes or additions.

4. Write tests to cover your changes if applicable.

5. Run tests to ensure everything is still working

6. Commit your changes and push to your forked repository.

7. Open a pull request with a clear description of your changes and their purpose.

### Code Style

Please follow the existing code style and conventions used in the project. Make sure your code is well-formatted, readable, and adheres to best practices.

### Issue Reporting

If you encounter any issues or have feature suggestions, please open an issue on the GitHub repository. Provide as much information as possible to help us understand and reproduce the problem or evaluate the suggested feature.

### License

 By contributing to this project, you agree that your contributions will be licensed under the MIT License.

I'm greatly appreciate your valuable contributions and thank you for your support!