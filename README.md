# FlareSolverr.js 🚀

<p align="center">
    <a href="https://opensource.org/licenses/MIT" style="margin-right: 20px;">
      <img src="https://img.shields.io/badge/License-MIT-blue.svg" style="width: 100%; height: auto;" alt="License: MIT">
    </a>
    <a href="https://www.npmjs.com/package/flaresolverr.js">
      <img src="https://badge.fury.io/js/flaresolverr.js.svg" style="width: 100%; height: auto;" alt="npm version">
      <img src="https://img.shields.io/npm/types/flaresolverr.js" style="width: 100%; height: auto;" alt="type definitions">
    </a>
    <a href="https://www.npmjs.com/package/flaresolverr.js?activeTab=dependents">
      <img src="https://img.shields.io/librariesio/dependents/npm/flaresolverr.js" style="width: 100%; height: auto;" alt="dependents">
    </a>
</p>

FlareSolverr.js is a lightweight JavaScript library that provides an API for interacting with the dockered [FlareSolverr](https://github.com/FlareSolverr/FlareSolverr) service. *FlareSolverr* is a solution designed to **bypass** challenges implemented by the Cloudflare anti-bot protection, allowing you to automate interactions with Cloudflare-protected websites.

## Installation

You can install FlareSolverr.js using npm(or yarn):

```bash
npm install flaresolverr.js # or yarn add flaresolverr.js
```

## Usage:

```typescript
const { FlareSolverr } = require('flaresolverr.js');

/*
Also you can import this library via esm syntax:
import { FlareSolverr } from "flaresolverr.js"
*/

// Initialize FlareSolverr with the API endpoint URL

const client = new FlareSolverr('http://localhost:8191/v1');

// Solve a Cloudflare challenge

const challengeResult = await client.get({
    url:new URL('http://example.com'),
    returnOnlyCookies:true
});

console.log(challengeResult);
/*
{
  status: 'ok',
  message: 'Challenge solved!',
  solution: {
    url: URL { href:"http://example.com", ... },
    status: 200,
    cookies: [ [Object], [Object], [Object], [Object], [Object] ],
    userAgent: 'Mozilla/5.0 (X11; Linux x86_64) ...'
  },
  startTimestamp: 1692311934966,
  endTimestamp: 1692311970847,
  version: '3.3.2'
}
*/
```

## Documentation

For detailed information on how to use FlareSolverr.js and available methods, please refer to the source code.

## To-do

- [ ] Tests

- [ ] Normal docs

## License

This project is licensed under the MIT License - see the LICENSE file for details.

Note: FlareSolverr.js is a third-party tool and is not affiliated with or endorsed by Cloudflare.

## Contributing
Feel free to contribute to this project!
