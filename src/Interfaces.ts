/**
 * Interface representing the settings of the FlareSolverr class.
 *
 * @remarks
 * The settings object may not be complete, in which case it will be combined with the default values.
 *
 * @internal
 */
export interface IOptions {
    /**
     * The limit value.
     */
    limit: number;
}

/**
 * Type representing all API methods.
 * @internal
 */
type TAPIMethodList = [
    'sessions.list',
    'sessions.create',
    'sessions.destroy',
    'request.get',
    'request.post',
];

/**
 * Type representing a single API method from the TAPIMethodList.
 * @internal
 */
type TAPIMethod = TAPIMethodList[number];

/**
 * Interface representing the data to send.
 */
export interface TPostData {
    /**
     * The command of the API method.
     */
    cmd: TAPIMethod;
    [param: string]: unknown;
}

/**
 * The base interface representing the response object from the FlareSolverr API endpoint.
 */
export interface IBaseAnswer {
    /**
     * The status of the response.
     */
    status: 'ok' | 'error';
    /**
     * The message of the response.
     */
    message: string;
    /**
     * The start timestamp of the response.
     */
    startTimestamp: Date;
    /**
     * The end timestamp of the response.
     */
    endTimestamp: Date;
    /**
     * The version of the FlareSolverr.
     */
    version: string;
}

/**
 * The interface representing the response object from the sessions.get API method.
 */
export interface ISessionsAnswer extends IBaseAnswer {
    /**
     * The list of sessions in the response.
     */
    sessions: string[];
}

/**
 * The interface representing the response object from the sessions.create API method.
 */
export interface ISessionAnswer extends IBaseAnswer {
    /**
     * Created session ID(UID).
     */
    session: string;
}

export interface IProxy {
    protocol: 'http' | 'socks5' | 'socks4';
    host: string;
    port: number | string;
    username?: string;
    password?: string;
}

/**
 * Interface representing the options for creating a session.
 */
export interface ISessionCreateOptions {
    /**
     * The session ID(UID) to be created.
     */
    session?: string;
    /**
     * The proxy object.
     */
    proxy?: IProxy;
}

/**
 * Type representing cookies.
 */
export type TCookies = Record<string, string | number | boolean>[];

/**
 * Interface representing the options for making a GET request.
 */
export interface IRequestGetOptions {
    /**
     * The URL to make the request to.
     */
    url: URL;
    /**
     * The session time-to-live in minutes.
     */
    session_ttl_minutes?: number;
    /**
     * The maximum timeout.
     */
    maxTimeout?: number;
    /**
     * The initial cookies.
     */
    cookies?: TCookies;
    /**
     * Whether to return only cookies.
     */
    returnOnlyCookies?: boolean;

    /**
     * The session ID(UID) to be used.
     */
    session?: string;

    /**
     * The proxy object without authorization.
     *
     * @remarks
     * Checkout {@link https://github.com/FlareSolverr/FlareSolverr#-requestget}
     */
    proxy?: Omit<IProxy, 'username' | 'password'>;
}

/**
 * Interface representing the options for making a POST request.
 */
export interface IRequestPostOptions extends IRequestGetOptions {
    /**
     * The POST data to send.
     */
    postData: Record<string, string | number | boolean>;
}

export interface IBaseRequestAnswer extends IBaseAnswer {
    solution: {
        /**
         * The status code of the response from target url.
         */
        status: number;
        /**
         * The headers of bypass session from target url.
         */
        headers?: Record<string, string>;
        /**
         * The response body of target url.
         */
        response?: string;
        /**
         * The cookies of bypass session.
         */
        cookies: TCookies;
        /**
         * The user agent of bypass session.
         */
        userAgent: string;
    };
}

/**
 * The raw interface representing a response object from the sessions.create or sessions.post API method.
 */
export interface IRawRequestAnswer extends IBaseRequestAnswer {
    solution: IBaseRequestAnswer['solution'] & { url: string };
}

/**
 * The interface representing a response object from the sessions.create or sessions.post API method.
 */
export interface IRequestAnswer extends IBaseRequestAnswer {
    solution: IBaseRequestAnswer['solution'] & { url: URL };
}
