import { IBaseAnswer } from './Interfaces.js';

export class InternalEndpointError extends Error {}

/**
 * UnknownError is thrown when an unknown error occurs in the API response.
 */
export class UnknownError extends Error {}

/**
 * BadRequestError is thrown when the API responds with a bad request error.
 */
export class BadRequestError extends Error {
    /**
     * @param response - The response object containing the error message.
     */
    constructor(readonly response: IBaseAnswer) {
        super(response.message.replace('Error:', '').trim());
    }
}
