import terser from '@rollup/plugin-terser';
import ts from '@rollup/plugin-typescript';
import eslint from '@rollup/plugin-eslint';
import clean from '@rollup-extras/plugin-clean';

const output_dir = 'dist';

const rollupConfig = {
    input: 'src/index.ts',
    output: [
        {
            dir: output_dir,
            format: 'esm',
            entryFileNames: '[name].mjs',
        },
        {
            dir: output_dir,
            format: 'commonjs',
            entryFileNames: '[name].cjs',
        },
    ],
    external: ['node:http'],
    plugins: [
        clean('dist'),
        eslint({
            fix: true,
            useEslintrc: true,
        }),
        ts({
            tsconfig: 'tsconfig.json',
        }),
        terser({
            format: {
                comments: 'some',
                beautify: true,
                ecma: 2020,
            },
            compress: true,
            mangle: false,
            module: true,
        }),
    ],
};

export default rollupConfig;
